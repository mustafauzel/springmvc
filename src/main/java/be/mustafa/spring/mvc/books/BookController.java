package be.mustafa.spring.mvc.books;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/book")
public class BookController {
    private static final String VIEW = "book/booklist";
    private BookRepository repo;

    @Autowired
    public void setRepo(BookRepository repo) {
        this.repo = repo;
    }

    @GetMapping
    public String getView(ModelMap model){
        model.addAttribute("allbooks", repo.getBooks());
        return VIEW;
    }

    @GetMapping(params = {"isbn"})
    public String getDetails(@RequestParam("isbn") String isbn, ModelMap model){
        model.addAttribute("bookdetails", repo.getBook(isbn));
        return "book/bookdetail";
    }



}
