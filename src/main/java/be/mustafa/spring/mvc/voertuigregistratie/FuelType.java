package be.mustafa.spring.mvc.voertuigregistratie;

import java.util.ArrayList;
import java.util.List;

public enum FuelType {
    GASOLINE,
    DIESEL,
    GAS,
    ELECTRIC;

    public static List<FuelType> valuesList(){
        List<FuelType> list = new ArrayList<>();
        list.add(GASOLINE);
        list.add(DIESEL);
        list.add(GAS);
        list.add(ELECTRIC);
        return list;
    }
}
