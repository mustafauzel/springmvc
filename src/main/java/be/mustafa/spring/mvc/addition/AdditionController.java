package be.mustafa.spring.mvc.addition;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/add")
public class AdditionController {
    @GetMapping
    public String loadPage(){
        return "add/addition";
    }

    @PostMapping
    public ModelAndView add(@RequestParam(value = "number1", required = false, defaultValue = "0") int number1,
                            @RequestParam(value = "number2", required = false, defaultValue = "0") int number2){
        int result = number1 + number2;
        return new ModelAndView("add/addition", "result", result);
    }
}
