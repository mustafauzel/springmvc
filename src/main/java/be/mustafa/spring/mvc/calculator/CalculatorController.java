package be.mustafa.spring.mvc.calculator;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.ApplicationScope;

@Controller
@RequestMapping("calc")
@ApplicationScope
@SessionAttributes("result")
public class CalculatorController {
    private static final String VIEW = "calculator/calculator";
    private int result;

    @GetMapping
    public String loadWebsite(ModelMap model){
        model.addAttribute("result", result);
        return VIEW;
    }

    @PostMapping(params = {"add"})
    public String add(@RequestParam("number1") int number, ModelMap model){
        result = (Integer)model.get("result") + number;
        model.addAttribute("result", result);
        return VIEW;
    }

    @PostMapping(params = {"sub"})
    public String subtract(@RequestParam("number1") int number, ModelMap model){
        result = (Integer)model.get("result") - number;
        model.addAttribute("result", result);
        return VIEW;
    }

    @PostMapping(params = {"mul"})
    public String multiply(@RequestParam("number1") int number, ModelMap model){
        result = (Integer)model.get("result") * number;
        model.addAttribute("result", result);
        return VIEW;
    }

    @PostMapping(params = {"div"})
    public String divide(@RequestParam("number1") int number, ModelMap model){
        result = (Integer) model.get("result") / number;
        model.addAttribute("result", result);
        return VIEW;
    }

    @PostMapping(params = {"reset"})
    public String reset(ModelMap model){
        model.addAttribute("result", 0);
        return VIEW;
    }
}
