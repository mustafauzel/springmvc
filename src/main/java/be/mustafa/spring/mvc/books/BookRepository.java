package be.mustafa.spring.mvc.books;

import java.util.List;

public interface BookRepository {
    List<Book> getBooks();
    Book getBook(String isbn);
}
