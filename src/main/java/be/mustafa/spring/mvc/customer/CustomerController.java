package be.mustafa.spring.mvc.customer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    private static final String VIEW = "customer/customers";

    @ModelAttribute("data")
    public Customer getData(){
        return new Customer();
    }

    @GetMapping
    public String handleGet(){
        return VIEW;
    }

    @PostMapping
    public String handleCustomer(@ModelAttribute("data") Customer customer){  //want customer heeft zelfde field namen als de html input namen.
        customer = new Customer();
        return VIEW;

    }
}
