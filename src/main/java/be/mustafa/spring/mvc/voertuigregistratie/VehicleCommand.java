package be.mustafa.spring.mvc.voertuigregistratie;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class VehicleCommand implements Serializable {
    @NotBlank
    private String brand;
    @NotBlank
    private String type;
    @Min(1971)
    private int year;
    @NotNull
    private FuelType fuel;
    @Min(1)
    private int power;
    @NotNull
    private TransmissionType transmission;
    @NotNull
    private boolean towbar;
    private List<String> options = new ArrayList<>();
    @NotNull
    @PastOrPresent
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate entryIntoService;
    @Pattern(regexp = "([0-9])+(-)+([a-zA-Z]{3})+(-)+([0-9]{3})") //"(?:([0-9])+(-)+([a-zA-Z]{3})+(-)+([0-9]{3})|([a-zA-Z0-9]{1,10}))"
    private String plate;
    @NotNull
    @DecimalMin(value = "1")
    @Digits(integer = 15,fraction = 2)
    private double price;

    public VehicleCommand() {
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public FuelType getFuel() {
        return fuel;
    }

    public void setFuel(FuelType fuel) {
        this.fuel = fuel;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public TransmissionType getTransmission() {
        return transmission;
    }

    public void setTransmission(TransmissionType transmission) {
        this.transmission = transmission;
    }

    public boolean isTowbar() {
        return towbar;
    }

    public void setTowbar(boolean towbar) {
        this.towbar = towbar;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public LocalDate getEntryIntoService() {
        return entryIntoService;
    }

    public void setEntryIntoService(LocalDate entryIntoService) {
        this.entryIntoService = entryIntoService;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getListedoptions(){
        StringJoiner str = new StringJoiner(", ");
        for(String el:options){
            str.add(el);
        }
        return str.toString();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", VehicleCommand.class.getSimpleName() + "[", "]")
                .add("brand='" + brand + "'")
                .add("type='" + type + "'")
                .add("year=" + year)
                .add("fuel=" + fuel)
                .add("power=" + power)
                .add("transmission=" + transmission)
                .add("towbar=" + towbar)
                .add("options=" + options)
                .add("entryIntoService=" + entryIntoService)
                .add("plate='" + plate + "'")
                .add("price=" + price)
                .toString();
    }
}
