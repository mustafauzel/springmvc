package be.mustafa.spring.mvc.product;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;

import javax.validation.constraints.PastOrPresent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@SessionScope
@RequestMapping("product")
public class ProductController {
    private static final String VIEW = "products/product";
    private List<String> productlist = new ArrayList<>();

    @ModelAttribute("products")
    public List<String> productList(){
        return productlist;
    }

    @GetMapping
    public String loadWebsite(){
        return VIEW;
    }

    @PostMapping(params = {"add"})
    public String addProduct(@RequestParam("addproduct") String productAdd){
        productlist.add(productAdd);
        return VIEW;

    }

    @PostMapping(params = {"del"})
    public String removeProduct(@RequestParam("productname") String productRemove){
        productlist.remove(productRemove);
        return VIEW;
    }

    @PostMapping(params = {"change"})
    public String changeProduct(@RequestParam("productname") String productChange, @RequestParam("index") int index){
        productlist.set(index, productChange);
        return VIEW;
    }




}
