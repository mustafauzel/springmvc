package be.mustafa.spring.mvc.counter;

import org.springframework.web.servlet.ModelAndView;


public interface CounterController {
    ModelAndView handle();
}
