package be.mustafa.spring.mvc.books;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/bookdetail")
public class BookListController {
    private BookRepository repo;

    @Autowired
    public void setRepo(BookRepository repo) {
        this.repo = repo;
    }

    @GetMapping
    public String loadPage(){
        return "book/bookdetail";
    }

}
