package be.mustafa.spring.mvc.age;

import org.springframework.http.MediaType;
import org.springframework.http.MediaTypeEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/selectage")
public class AgeController {
    /*@GetMapping(params = "action=cancel")
    public String handleCancel(){
        return "age/cancel";
    }

    @GetMapping(params = {"age=kid", "action=Submit"})
    public String handleKid(){
        return "age/children";
    }
    @GetMapping(params = {"age=teen", "action=Submit"})
    public String handleTeen(){
        return "age/teenagers";
    }
    @GetMapping(params = {"age=adult", "action=Submit"})
    public String handleAdult(){
        return "age/adults";
    }

    @GetMapping
    public ModelAndView selectAge(){
        return new ModelAndView("age/selectage");
    }*/

    @GetMapping
    public ModelAndView selectAge(){
        return new ModelAndView("age/selectage");
    }

    @PostMapping(params = {"action=Submit", "age=kid"})
    public String getKid(){
        return "age/children";
    }

    @PostMapping(params = {"action=Submit", "age=teen"})
    public String getTeen(){
        return "age/teenagers";
    }

    @PostMapping(params = {"action=Submit", "age=adult"})
    public String getAdult(){
        return "age/adults";
    }

    @PostMapping(params = {"action=cancel"})
    public String getCancel(){
        return "age/cancel";
    }

}
