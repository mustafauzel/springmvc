package be.mustafa.spring.mvc.hello;

import org.springframework.web.servlet.ModelAndView;

public interface HelloController {
    ModelAndView handleHello();
}
