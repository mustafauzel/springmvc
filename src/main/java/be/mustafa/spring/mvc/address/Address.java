package be.mustafa.spring.mvc.address;

import java.io.Serializable;

public class Address implements Serializable {
    private String firstName;
    private String lastName;
    private String addressWithNumber;
    private String zipCode;
    private String city;
    private String country;

    public Address(String firstName, String lastName, String addressWithNumber, String zipCode, String city, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.addressWithNumber = addressWithNumber;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }


}
