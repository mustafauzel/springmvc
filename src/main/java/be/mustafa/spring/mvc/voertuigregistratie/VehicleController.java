package be.mustafa.spring.mvc.voertuigregistratie;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Objects;

@Controller
@RequestMapping("/vehicle")
public class VehicleController {
    @GetMapping
    public String loadPage(@ModelAttribute("myform") VehicleCommand form){
        return "vehicle/registration";
    }

    @PostMapping
    public String register(@Valid @ModelAttribute("myform") VehicleCommand form, BindingResult br){
        if(br.hasErrors()){
            for(String code : br.getFieldError().getCodes()){
                System.out.println(code);
            }
            System.out.println("ERROR");
            return "vehicle/registration";
        }else{
            return "vehicle/confirm";
        }
    }
}
