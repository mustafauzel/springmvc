package be.mustafa.spring.mvc.books;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookRepositoryMemoryImpl implements BookRepository{
    private Map<String, Book> booklist = new HashMap<>();

    @PostConstruct
    public void addBooks(){
        booklist.put("123456789", new Book("123456789", "Barry Poter", "Mustafa", 9.99, 50));
        booklist.put("222222222", new Book("222222222", "Clean Code", "Uncle Bob", 59.99, 10));

    }

    @Override
    public List<Book> getBooks() {
        return new ArrayList<>(booklist.values());
    }

    @Override
    public Book getBook(String isbn) {
        return booklist.get(isbn);
    }
}
