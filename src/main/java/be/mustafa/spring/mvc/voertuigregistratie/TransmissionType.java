package be.mustafa.spring.mvc.voertuigregistratie;

import java.util.ArrayList;
import java.util.List;

public enum TransmissionType {
    MANUAL,
    AUTOMATIC;

    public static List<TransmissionType> valueList(){
        List<TransmissionType> list = new ArrayList<>();
        list.add(MANUAL);
        list.add(AUTOMATIC);
        return list;
    }
}
