package be.mustafa.spring.mvc.hello;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Service
public class HelloService implements Hello {
    @Override
    public String sayHello() {
        return "Hello World!";
    }
}
